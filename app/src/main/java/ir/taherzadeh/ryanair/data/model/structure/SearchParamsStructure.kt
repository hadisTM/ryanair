package ir.taherzadeh.ryanair.data.model.structure

import java.io.Serializable

class SearchParamsStructure : Serializable {
    var departureDate: String = ""
    var originName: String = ""
    var originCode: String = ""
    var destinationName: String = ""
    var destinationCode: String = ""
    var adultNum: Int = 0
    var childrenNum: Int = 0
    var teensNum: Int = 0
}