package ir.taherzadeh.ryanair.data.api

import ir.taherzadeh.ryanair.data.model.response.AvailableStationsResponseItem
import ir.taherzadeh.ryanair.data.model.response.FlightResultResponseItem
import retrofit2.Response
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface ApiHelper {
    suspend fun getAvailableStations(): Response<AvailableStationsResponseItem>

    suspend fun getFlightResult(@Url url:String, @QueryMap params: MutableMap<String, Any>): Response<FlightResultResponseItem>
}