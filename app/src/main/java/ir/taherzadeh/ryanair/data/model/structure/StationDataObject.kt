package ir.taherzadeh.ryanair.data.model.structure

data class StationDataObject(
    val code: String,
    val name: String,
    val alternateName: String?,
    val alias: List<String>,
    val countryCode: String,
    val countryName: String,
    val countryAlias: String?,
    val countryGroupCode: String,
    val countryGroupName: String,
    val timeZoneCode: String,
    val latitude: String,
    val longitude: String,
    val mobileBoardingPass: Boolean,
    val markets: List<MarketDataObject>,
    val notices: String?
)