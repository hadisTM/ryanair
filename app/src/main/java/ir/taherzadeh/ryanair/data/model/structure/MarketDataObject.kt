package ir.taherzadeh.ryanair.data.model.structure

data class MarketDataObject(
    val code: String,
    val group: String
)