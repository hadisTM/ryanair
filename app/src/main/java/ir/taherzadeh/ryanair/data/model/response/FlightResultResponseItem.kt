package ir.taherzadeh.ryanair.data.model.response

import ir.taherzadeh.ryanair.data.model.structure.TripDataObject

class FlightResultResponseItem(var error: String) {
    val currency: String = ""
    val trips: List<TripDataObject>? = null

    //parameters in below should be eliminated because these are never used!
//    val termsOfUse: String = ""
//    val currPrecision: Int = 0
//    val serverTimeUTC: String = ""

}