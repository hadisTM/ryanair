package ir.taherzadeh.ryanair.data.di

import ir.taherzadeh.ryanair.data.repository.ApiRepo
import org.koin.dsl.module

val repoModule = module {
    single {
        ApiRepo(get())
    }
}