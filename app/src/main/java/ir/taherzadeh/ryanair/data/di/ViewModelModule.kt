package ir.taherzadeh.ryanair.data.di

import ir.taherzadeh.ryanair.view.ui.flight_fragment.FlightViewModel
import ir.taherzadeh.ryanair.view.ui.station_fragment.StationViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        StationViewModel(
            get()
        )
    }
    viewModel {
        FlightViewModel(get())
    }
}