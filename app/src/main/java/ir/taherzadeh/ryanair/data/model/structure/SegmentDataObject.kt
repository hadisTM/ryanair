package ir.taherzadeh.ryanair.data.model.structure

data class SegmentDataObject(
    val segmentNr: String,
    val origin: String,
    val destination: String,
    val flightNumber: String,
    val time: List<String>,
    val timeUTC: List<String>,
    val duration: String
)