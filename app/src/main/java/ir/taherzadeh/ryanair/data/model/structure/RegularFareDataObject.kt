package ir.taherzadeh.ryanair.data.model.structure

data class RegularFareDataObject(
    val fareKey: String,
    val fareClass: String,
    val fares: List<FareDataObject>
)