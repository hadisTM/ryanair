package ir.taherzadeh.ryanair.data.model.response
import ir.taherzadeh.ryanair.data.model.structure.StationDataObject

class AvailableStationsResponseItem(var error: String) {
    val stations: List<StationDataObject>? = null

}