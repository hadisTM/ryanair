package ir.taherzadeh.ryanair.data.model.structure

data class DateDataObject(
    val dateOut: String,
    val flights: List<FlightDataObject>
)