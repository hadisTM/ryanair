package ir.taherzadeh.ryanair.data.repository

import ir.taherzadeh.ryanair.data.api.ApiHelper

class ApiRepo(private val apiHelper: ApiHelper) {
    private val flightResultBaseUrl = "https://www.ryanair.com/api/booking/v4/en-gb/Availability?"

    suspend fun getAvailableStations() = apiHelper.getAvailableStations()

    suspend fun searchFlight(params: MutableMap<String, Any>) =
        apiHelper.getFlightResult(flightResultBaseUrl, params)

}

