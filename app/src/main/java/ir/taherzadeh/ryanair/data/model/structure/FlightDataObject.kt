package ir.taherzadeh.ryanair.data.model.structure

data class FlightDataObject(
    val faresLeft: Float,
    val flightKey: String,
    val infantsLeft: String,
    val regularFare: RegularFareDataObject,
    val segments: List<SegmentDataObject>,
    val flightNumber: String,
    val time: List<String>,
    val timeUTC: List<String>,
    val duration: String
)