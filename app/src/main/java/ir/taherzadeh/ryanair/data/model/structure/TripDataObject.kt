package ir.taherzadeh.ryanair.data.model.structure

data class TripDataObject(
    val origin: String,
    val originName: String,
    val destination: String,
    val destinationName: String,
    val dates: List<DateDataObject>
)