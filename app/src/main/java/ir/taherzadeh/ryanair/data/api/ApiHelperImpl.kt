package ir.taherzadeh.ryanair.data.api

import ir.taherzadeh.ryanair.data.model.response.AvailableStationsResponseItem
import ir.taherzadeh.ryanair.data.model.response.FlightResultResponseItem
import retrofit2.Response

class ApiHelperImpl(private  val apiService: ApiService):ApiHelper {
    override suspend fun getAvailableStations():
            Response<AvailableStationsResponseItem> =apiService.getAvailableStations()

    override suspend fun getFlightResult(url: String,params: MutableMap<String, Any>):
            Response<FlightResultResponseItem> =apiService.getFlightResult(url,params)

}