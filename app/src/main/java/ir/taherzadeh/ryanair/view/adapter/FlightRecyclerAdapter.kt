package ir.taherzadeh.ryanair.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.taherzadeh.ryanair.R
import ir.taherzadeh.ryanair.data.model.structure.FlightDataObject
import kotlinx.android.synthetic.main.item_flight.view.*


class FlightRecyclerAdapter(
    private val context: Context,
    private val items: ArrayList<FlightDataObject>,
    private var constValues: MutableMap<String, String>,
    private val listener: OnItemClickListener
) :


    RecyclerView.Adapter<FlightRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_flight, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.txtFlightDate.text = "Date: ${constValues["dateOut"]}"
        holder.txtFlightNumber.text = "Flight number: ${item.flightNumber}"
        holder.txtDuration.text = "Duration: ${item.duration}"
        holder.txtFare.text =
            "Price: ${item.regularFare.fares[0].amount} ${constValues["currency"]}"

        holder.itemView.setOnClickListener {
            listener.setOnItemClickListener(item)
        }
    }


    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtFlightDate: TextView = view.txtFlightDate
        val txtFlightNumber: TextView = view.txtFlightNumber
        val txtDuration: TextView = view.txtDuration
        val txtFare: TextView = view.txtFare
    }

    interface OnItemClickListener {
        fun setOnItemClickListener(item: FlightDataObject)
    }

    fun addData(array: List<FlightDataObject>, _constValues: MutableMap<String, String>) {
        items.addAll(array)
        constValues = _constValues
    }
}