package ir.taherzadeh.ryanair.view.ui.flight_fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.taherzadeh.ryanair.data.model.response.FlightResultResponseItem
import ir.taherzadeh.ryanair.data.repository.ApiRepo
import kotlinx.coroutines.launch

class FlightViewModel(private val mApiRepo: ApiRepo) : ViewModel() {

    private val flightResult = MutableLiveData<FlightResultResponseItem>()

    fun searchFlight(params: MutableMap<String, Any>): LiveData<FlightResultResponseItem> {
        viewModelScope.launch {
            mApiRepo.searchFlight(params).let {
                if (it.isSuccessful)
                    flightResult.postValue(it.body())
                else
                    flightResult.postValue(FlightResultResponseItem(it.errorBody().toString()))
            }
        }
        return flightResult

    }
}
