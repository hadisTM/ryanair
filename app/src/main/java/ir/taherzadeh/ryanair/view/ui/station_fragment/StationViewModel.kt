package ir.taherzadeh.ryanair.view.ui.station_fragment

import androidx.lifecycle.*
import ir.taherzadeh.ryanair.data.model.response.AvailableStationsResponseItem
import ir.taherzadeh.ryanair.data.repository.ApiRepo
import kotlinx.coroutines.launch

class StationViewModel(private val mApiRepo: ApiRepo) : ViewModel() {

    private val _stations = MutableLiveData<AvailableStationsResponseItem>()
    val stations: LiveData<AvailableStationsResponseItem>
        get() = _stations
    init {
        fetchStation()
    }

    private fun fetchStation() {
        viewModelScope.launch {
            mApiRepo.getAvailableStations().let {
                if (it.isSuccessful)
                    _stations.postValue(it.body())
                else _stations.postValue(AvailableStationsResponseItem(it.errorBody().toString()))
            }
        }
    }

}
