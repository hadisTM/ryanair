package ir.taherzadeh.ryanair.view.ui

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ir.taherzadeh.ryanair.R


class MainActivity : AppCompatActivity() {

    private var dialog: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showLoading() {
        dialog = AlertDialog.Builder(this)
            .setView(R.layout.progress_dialog)
            .show()
    }

    fun dismissLoading() {
        if (dialog != null && dialog!!.isShowing)
            dialog!!.dismiss()
    }

}
