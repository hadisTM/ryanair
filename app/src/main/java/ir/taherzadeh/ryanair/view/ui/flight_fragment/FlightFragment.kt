package ir.taherzadeh.ryanair.view.ui.flight_fragment

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import ir.taherzadeh.ryanair.R
import ir.taherzadeh.ryanair.data.model.response.FlightResultResponseItem
import ir.taherzadeh.ryanair.data.model.structure.DateDataObject
import ir.taherzadeh.ryanair.data.model.structure.FlightDataObject
import ir.taherzadeh.ryanair.data.model.structure.SearchParamsStructure
import ir.taherzadeh.ryanair.view.adapter.FlightRecyclerAdapter
import ir.taherzadeh.ryanair.view.ui.MainActivity
import kotlinx.android.synthetic.main.flight_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class FlightFragment : Fragment() {
    private lateinit var searchParam: SearchParamsStructure
    private lateinit var adapter: FlightRecyclerAdapter

    // Lazy Inject ViewModel
    private val viewModel: FlightViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchParam = arguments?.getSerializable("search_params")!! as SearchParamsStructure
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.flight_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showLoading()
        setupRecycler()
        setToolbarName()
        setupObserve()
    }

    private fun setupRecycler() {
        adapter = FlightRecyclerAdapter(
            this.requireContext(),
            arrayListOf()
            , hashMapOf()
            , object : FlightRecyclerAdapter.OnItemClickListener {
                override fun setOnItemClickListener(item: FlightDataObject) {
                    showDetails(
                        item.infantsLeft, item.regularFare.fareClass,
                        item.regularFare.fares[0].discountInPercent //the position of 'fares' array
                        // is set to 0 because all 'regularFare.fares' arrays have only 1 size,
                        //otherwise it would have handled like array.
                    )
                }
            })
        rclTrips.layoutManager =
            LinearLayoutManager(rclTrips.context, LinearLayoutManager.VERTICAL, false)
        rclTrips.setHasFixedSize(true)
        rclTrips.adapter = adapter

    }

    private fun setupObserve() {
        viewModel.searchFlight(setSearchFlightParams()).observe(viewLifecycleOwner, Observer {
            try {
                (activity as MainActivity).dismissLoading()
                if (it.trips != null)
                    renderList(it)
                else
                    Toast.makeText(
                        activity,
                        getString(R.string.error_get_flight),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                i("LOG", it.error)
            } catch (e: Exception) {
                e.printStackTrace()
            }


        })
    }

    private fun setToolbarName() {
        txtOriginName.text = searchParam.originName
        txtDestinationName.text = searchParam.destinationName
    }

    private fun setSearchFlightParams(): MutableMap<String, Any> {
        val params: MutableMap<String, Any> = HashMap()
        params["dateout"] = searchParam.departureDate
        params["roundtrip"] = false
        params["origin"] = searchParam.originCode
        params["destination"] = searchParam.destinationCode
        params["flexdaysout"] = 3
        params["flexdaysin"] = 3
        params["flexdaysbeforeout"] = 3
        params["flexdaysbeforein"] = 3
        params["adt"] = searchParam.adultNum
        params["chd"] = searchParam.childrenNum
        params["teen"] = searchParam.teensNum
        params["inf"] = 0
        params["ToUs"] = "AGREED"
        params["Disc"] = 0
//        params["datein"] = "2020-12-31" it's not a roundtrip flight
        return params
    }

    private fun renderList(it: FlightResultResponseItem) {
        val constValues: MutableMap<String, String> = HashMap()
        constValues["currency"] = it.currency
        var selectedDate: DateDataObject? = null
        for (i in it.trips!!.indices) {
            for (element in it.trips[i].dates) {
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val date = sdf.parse(element.dateOut)
                val formattedDate = sdf.format(date!!)
                if (formattedDate == searchParam.departureDate) {
                    selectedDate = element
                    constValues["dateOut"] = date.toString()
                }
            }
        }
        adapter.addData(selectedDate?.flights!!, constValues)
        adapter.notifyDataSetChanged()


    }

    private fun showDetails(infantsLeft: String, fareClass: String, discountInPercent: Int) {
        val details = "infantsLeft is: $infantsLeft \n" +
                "FareClass is: $fareClass \n" +
                "Discount In Percent is: $discountInPercent"
        AlertDialog.Builder(context)
            .setTitle(getString(R.string.flight_info))
            .setMessage(details)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setCancelable(false)
            .setPositiveButton(
                getString(R.string.ok)
            ) { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()

    }


}

