package ir.taherzadeh.ryanair.view.ui.station_fragment

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import ir.taherzadeh.ryanair.R
import ir.taherzadeh.ryanair.data.model.structure.SearchParamsStructure
import ir.taherzadeh.ryanair.data.model.structure.StationDataObject
import ir.taherzadeh.ryanair.view.ui.MainActivity
import kotlinx.android.synthetic.main.station_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.Exception
import java.util.*

class StationFragment : Fragment(), View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private var selectedDate = String()
    private var stationNameList = mutableListOf<String>()
    private lateinit var stationList: List<StationDataObject>

    // Lazy Inject ViewModel
    private val viewModel: StationViewModel by viewModel()
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.station_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        (activity as MainActivity).showLoading()
        setupObserver()
        txtDepartureDate.setOnClickListener(this)
        btnSearch.setOnClickListener(this)
    }

    private fun setupObserver() {
        viewModel.stations.observe(viewLifecycleOwner, Observer {
            try {
                (activity as MainActivity).dismissLoading()
                if (it.stations != null)
                    setSpinners(it.stations)
                else {
                    Toast.makeText(activity, "Error while getting stations!", Toast.LENGTH_SHORT)
                        .show()
                    Log.e("LOG", it.error)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun setSpinners(stations: List<StationDataObject>) {
        stationList = stations
        for (i in 0 until stations.size.minus(1)) {
            stationNameList.add(stations[i].name)
        }
        val adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, stationNameList)
        spnOriginStation.adapter = adapter
        spnDestinationStation.adapter = adapter
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.txtDepartureDate -> openDatePickerDialog()
            R.id.btnSearch -> searchFlight()
        }
    }

    private fun searchFlight() {
        if (TextUtils.isEmpty(selectedDate)) {
            Toast.makeText(activity, getString(R.string.enter_departure_date), Toast.LENGTH_SHORT)
                .show()
            return
        }

        val searchParamsStructure = SearchParamsStructure()
        searchParamsStructure.departureDate = txtDepartureDate.text.toString()
        searchParamsStructure.originCode = stationList[spnOriginStation.selectedItemPosition].code
        searchParamsStructure.originName = spnOriginStation.selectedItem.toString()
        searchParamsStructure.destinationCode =
            stationList[spnDestinationStation.selectedItemPosition].code
        searchParamsStructure.destinationName = spnDestinationStation.selectedItem.toString()
        searchParamsStructure.adultNum = edtAdultNumber.text.toString().toInt()
        searchParamsStructure.childrenNum = edtChildrenNumber.text.toString().toInt()
        searchParamsStructure.teensNum = edtTeensNumber.text.toString().toInt()

        val bundle = bundleOf("search_params" to searchParamsStructure)
        navController.navigate(R.id.action_stationFragment_to_flightFragment, bundle)
    }

    private fun openDatePickerDialog() {
        val calendar: Calendar = Calendar.getInstance(TimeZone.getDefault())
        DatePickerDialog(
            requireContext(), this,
            calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).show()

    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        selectedDate = "$year-${month.plus(1)}-$day"
        txtDepartureDate.text = selectedDate
    }

}
