package ir.taherzadeh.ryanair

import android.app.Application
import ir.taherzadeh.ryanair.data.di.appModule
import ir.taherzadeh.ryanair.data.di.repoModule
import ir.taherzadeh.ryanair.data.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, repoModule, viewModelModule))
        }
    }

}