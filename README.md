An example using single activity, fragments and arguments with Navigation Component (Kotlin).
This repository implements Koin DI in MVVM architecture & Live Data.
Retrofit2 is used to handle Restful APis.
Common use-cases of Kotlin Coroutines in Android has been implemented in this project.